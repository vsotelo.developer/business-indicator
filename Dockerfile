FROM openjdk:8-jdk-alpine
EXPOSE 5202
COPY etc/worker_format.xlsx /etc/worker_format.xlsx
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
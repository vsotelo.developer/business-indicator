package com.utp.business.indicator.factory;

import com.utp.business.indicator.model.User;

import java.util.HashMap;
import java.util.Optional;

public class BDFake {

    private static HashMap<String, User> USERS_SYSTEM = null;

    /**
     * @return get All Users
     */
    public static HashMap<String, User> getAllUsers() {
        if (USERS_SYSTEM != null) {
            return USERS_SYSTEM;
        }
        USERS_SYSTEM = new HashMap<>();
        return USERS_SYSTEM;
    }

    /**
     * @param user user to Add
     */
    public static void addUser(User user) {
        getAllUsers().put(user.getDocumentNumber(), user);
    }

    /**
     * * @param documentNumber
     * @return optional User
     */
    public static Optional<User> findByDocumentNumber(String documentNumber) {
        return getAllUsers().containsKey(documentNumber)
                ? Optional.of(getAllUsers().get(documentNumber))
                : Optional.empty();
    }

}

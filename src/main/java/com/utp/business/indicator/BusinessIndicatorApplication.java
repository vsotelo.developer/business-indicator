package com.utp.business.indicator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BusinessIndicatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusinessIndicatorApplication.class, args);
	}

}

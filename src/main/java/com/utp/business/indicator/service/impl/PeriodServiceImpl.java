package com.utp.business.indicator.service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.utp.business.indicator.controller.expose.*;
import com.utp.business.indicator.factory.BDFake;
import com.utp.business.indicator.model.*;
import com.utp.business.indicator.model.IndicatorValue;
import com.utp.business.indicator.service.PeriodService;
import com.utp.business.indicator.util.DateUtils;
import com.utp.business.indicator.util.ServiceConstants;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PeriodServiceImpl implements PeriodService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PeriodServiceImpl.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(ServiceConstants.LOGGER_FORMAT_DATE).create();


    @Override
    public Period insertPeriod(PeriodRequest periodRequest, String documentNumber) throws Exception {
        LOGGER.info("insertPeriod() -> period,  documentNumber: {}, {}", GSON.toJson(periodRequest), documentNumber);

        Optional<User> optionalUser = BDFake.findByDocumentNumber(documentNumber);

        if (!optionalUser.isPresent())
            throw new Exception("Usuario No Existe");

        if (isDifferentMonth(periodRequest)) {
            throw new Exception("El periodo no puede tener un rango de meses diferentes");
        }

        if (CollectionUtils.isEmpty(optionalUser.get().getWorkers())) {
            throw new Exception("El periodo no puede iniciar, debe tener al menos 1 trabajador registrado");
        }

        if (CollectionUtils.isEmpty(optionalUser.get().getIndicators())) {
            throw new Exception("El periodo no puede iniciar, debe tener al menos 1 indicador registrado");
        }

        List<Worker> workers = optionalUser.get().getWorkers();
        List<Indicator> indicators = optionalUser.get().getIndicators();

        List<Period> listPeriods = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(optionalUser.get().getPeriods())) {
            listPeriods.addAll(optionalUser.get().getPeriods());
        }

        Period period = Period.builder().id(UUID.randomUUID().toString())
                .name(periodRequest.getName())
                .dateInit(parseStringToDate(periodRequest.getDateInit()))
                .dateEnd(parseStringToDate(periodRequest.getDateEnd()))
                .listIndicatorsPeriod(optionalUser.get().getIndicators())
                // Inicio Detalle
                .periodDetails(getPeriodDetails(periodRequest, workers, indicators))
                // Fin Detalle
                .build();

        listPeriods.add(period);


        optionalUser.get().setPeriods(listPeriods);
        BDFake.addUser(optionalUser.get());

        return period;

    }

    private boolean isDifferentMonth(PeriodRequest periodRequest) throws Exception {
        return DateUtils.dateToCalendar(parseStringToDate(periodRequest.getDateInit())).get(Calendar.MONTH) != DateUtils.dateToCalendar(parseStringToDate(periodRequest.getDateEnd())).get(Calendar.MONTH);
    }

    @Override
    public List<Period> getAllPeriods(String documentNumber) throws Exception {
        LOGGER.info("getAllPeriods() -> documentNumber: {}", documentNumber);

        Optional<User> optionalUser = BDFake.findByDocumentNumber(documentNumber);

        if (!optionalUser.isPresent())
            //throw new Exception("Usuario No Existe");
            return Collections.emptyList();

        if (CollectionUtils.isNotEmpty(optionalUser.get().getPeriods())) {
            return optionalUser.get().getPeriods()
                    .stream().sorted(Comparator.comparing(Period::getDateInit).reversed())
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    @Override
    public boolean updateMasiveIndicators(List<IndicatorValueUpdateRequest> indicatorValueUpdateRequests, String documentNumber) throws Exception {
        LOGGER.info("updateMasiveIndicators() -> indicatorValueUpdateRequests, documentNumber: {}, {}",
                GSON.toJson(indicatorValueUpdateRequests), documentNumber);

        Optional<User> optionalUser = BDFake.findByDocumentNumber(documentNumber);

        if (!optionalUser.isPresent())
            throw new Exception("Usuario No Existe");

        if (CollectionUtils.isEmpty(optionalUser.get().getPeriods()))
            throw new Exception("Debe tener al menos 1 periodo registrado");


        indicatorValueUpdateRequests.forEach(indicatorValueUpdateRequest -> {
            Optional<Period> periodSelectedOptional = optionalUser.get().getPeriods()
                    .stream().filter(period -> period.getId().equals(indicatorValueUpdateRequest.getPeriodId()))
                    .findFirst();
            if (periodSelectedOptional.isPresent()) {
                Period period = periodSelectedOptional.get();
                Optional<PeriodDetail> periodDetailOptional = period.getPeriodDetails()
                        .stream().filter(periodDetail -> periodDetail.getId().equals(indicatorValueUpdateRequest.getPeriodDetailId()))
                        .findFirst();
                if (periodDetailOptional.isPresent()) {
                    PeriodDetail periodDetail = periodDetailOptional.get();
                    Optional<WorkerDetailPeriod> workerDetailPeriodOptional = periodDetail.getWorkerDetailPeriods()
                            .stream().filter(workerDetailPeriod -> workerDetailPeriod.getId().equals(indicatorValueUpdateRequest.getWorkerPeriodDetailId()))
                            .findFirst();
                    if (workerDetailPeriodOptional.isPresent()) {
                        WorkerDetailPeriod workerDetailPeriod = workerDetailPeriodOptional.get();
                        Optional<IndicatorValue> optionalIndicatorValue = workerDetailPeriod.getIndicatorValues()
                                .stream().filter(indicatorValue -> indicatorValue.getId().equals(indicatorValueUpdateRequest.getIndicatorValueId()))
                                .findFirst();
                        if (optionalIndicatorValue.isPresent()) {
                            IndicatorValue indicatorValue = optionalIndicatorValue.get();
                            indicatorValue.setValue(indicatorValueUpdateRequest.getValue());
                        }
                    }
                }
            }

            BDFake.addUser(optionalUser.get());
        });

        return true;
    }

    @Override
    public List<PeriodAverageRs> listAverage(String documentNumber) throws Exception {
        LOGGER.info("listAverage() -> documentNumber: {}", documentNumber);
        Optional<User> optionalUser = BDFake.findByDocumentNumber(documentNumber);

        if (!optionalUser.isPresent())
            //throw new Exception("Usuario No Existe");
            return Collections.emptyList();

        if (CollectionUtils.isEmpty(optionalUser.get().getPeriods())) {
            //throw new Exception("No hay periodos registrados");
            return Collections.emptyList();
        }

        List<Period> periodList = optionalUser.get().getPeriods()
                .stream().sorted(Comparator.comparing(Period::getDateInit))
                .collect(Collectors.toList());

        List<PeriodAverageRs> listPeriodAverageRs = new ArrayList<>();

        int index = 0;
        for (Period period : periodList) {

            PeriodAverageRs periodAverageRs = PeriodAverageRs.builder()
                    .periodId(period.getId())
                    .index(index)
                    .value(period.getAverageByDays())
                    .periodName(period.getName())
                    .build();

            listPeriodAverageRs.add(periodAverageRs);
            index++;
        }

        return listPeriodAverageRs;
    }

    @Override
    public List<IndicatorValueRs> listValuesIndicatorsByPeriodId(String documentNumber, String periodId) throws Exception {
        LOGGER.info("listAverage() -> documentNumber, periodId, driverId: {}, {}", documentNumber, periodId);
        Optional<User> optionalUser = BDFake.findByDocumentNumber(documentNumber);

        if (!optionalUser.isPresent())
            //throw new Exception("Usuario No Existe");
            return Collections.emptyList();

        if (CollectionUtils.isEmpty(optionalUser.get().getPeriods())) {
            //throw new Exception("No hay periodos registrados");
            return Collections.emptyList();
        }

        Optional<Period> optionalPeriod = optionalUser.get().getPeriods()
                .stream().filter(period -> period.getId().equals(periodId)).findFirst();

        List<IndicatorValueRs> listIndicatorValue = new ArrayList<>();
        if (optionalPeriod.isPresent()) {

            int index = 0;
            for (Indicator indicator :optionalPeriod.get().getListIndicatorsPeriod()) {

                //double sumaTotalDelDia = 0;
                double averageDeIndicadorPorTodosLosTrabajadores = 0;
                for (PeriodDetail periodDetail : optionalPeriod.get().getPeriodDetails()) {

                    // obtenemos todos los trabajadores de ese dia
                    double valorTotalDelIndicador = 0;
                    for (WorkerDetailPeriod workerDetailPeriod : periodDetail.getWorkerDetailPeriods()) {
                        // Obtenemos el valor del indicador del dia
                        for (IndicatorValue indicatorValue : workerDetailPeriod.getIndicatorValues()) {
                            if (indicatorValue.getIndicator().getId().equals(indicator.getId())) {
                                valorTotalDelIndicador = valorTotalDelIndicador + indicatorValue.getValue();
                            }
                        }
                    }
                    // dividimos entre el numero de trabajadores para obtener el promedio
                    averageDeIndicadorPorTodosLosTrabajadores = (valorTotalDelIndicador / periodDetail.getWorkerDetailPeriods().size()) + averageDeIndicadorPorTodosLosTrabajadores;
                }

                double promedioTotal = averageDeIndicadorPorTodosLosTrabajadores / optionalPeriod.get().getPeriodDetails().size();

                listIndicatorValue.add(IndicatorValueRs.builder()
                        .index(index)
                        .indicator(indicator)
                        .average(promedioTotal)
                        .build());
                index++;
            }

            return listIndicatorValue;

        }

        return Collections.emptyList();
    }

    private List<PeriodDetail> getPeriodDetails(PeriodRequest periodRequest, List<Worker> workers, List<Indicator> indicators) throws Exception {
        List<PeriodDetail> listReturn = new ArrayList<>();
        final long DAY_MILLISECONDS = 86400000L;
        int numDias = DateUtils.dateToCalendar(parseStringToDate(periodRequest.getDateEnd())).get(Calendar.DAY_OF_MONTH) - DateUtils.dateToCalendar(parseStringToDate(periodRequest.getDateInit())).get(Calendar.DAY_OF_MONTH);
        for (int i = 0; i <= numDias; i++) {
            long dia = DAY_MILLISECONDS * i;
            Date dateGeneric = new Date(parseStringToDate(periodRequest.getDateInit()).getTime() + dia);
            listReturn.add(
                    PeriodDetail.builder()
                            .id(UUID.randomUUID().toString())
                            .date(dateGeneric)
                            .workerDetailPeriods(manageDetailsPeriosWorkers(workers, indicators))
                            .build()
            );
        }
        return listReturn;
    }

    private List<WorkerDetailPeriod> manageDetailsPeriosWorkers(List<Worker> workers, List<Indicator> indicators) {
        List<WorkerDetailPeriod> workerDetailPeriods = new ArrayList<>();
        workers.forEach(worker -> {
            workerDetailPeriods.add(WorkerDetailPeriod.builder()
                    .id(UUID.randomUUID().toString())
                    .worker(worker)
                    .indicatorValues(getListIndicator(indicators))
                    .build());
        });

        return workerDetailPeriods;
    }

    private List<IndicatorValue> getListIndicator(List<Indicator> indicators) {
        List<IndicatorValue> indicatorValues = new ArrayList<>();
        indicators.forEach(indicator -> {
            indicatorValues.add(IndicatorValue.builder()
                    .id(UUID.randomUUID().toString())
                    .indicator(indicator)
                    .value(100)
                    .build());
        });

        return indicatorValues;
    }


    private PeriodResponse parsePeriod(Period period) {
        return PeriodResponse.builder()
                .id(period.getId())
                .name(period.getName())
                .dateInit(period.getDateInit())
                .dateEnd(period.getDateEnd())
                .periodDetails(parseListPeriod(period.getPeriodDetails()))
                .build();
    }

    private List<PeriodDetailResponse> parseListPeriod(List<PeriodDetail> periodDetailList) {
        return periodDetailList.stream()
                .map(periodDetail -> PeriodDetailResponse.builder()
                        .id(periodDetail.getId())
                        .date(periodDetail.getDate()).build())
                .collect(Collectors.toList());
    }

    private Date parseStringToDate(String date) throws Exception {
        return new SimpleDateFormat("yyyy-MM-dd").parse(date);
    }

}

package com.utp.business.indicator.service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.utp.business.indicator.controller.expose.IndicatorRq;
import com.utp.business.indicator.controller.expose.IndicatorRs;
import com.utp.business.indicator.factory.BDFake;
import com.utp.business.indicator.model.Indicator;
import com.utp.business.indicator.model.User;
import com.utp.business.indicator.service.IndicatorService;
import com.utp.business.indicator.util.ServiceConstants;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class IndicatorServiceImpl implements IndicatorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndicatorServiceImpl.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(ServiceConstants.LOGGER_FORMAT_DATE).create();

    @Override
    public List<IndicatorRs> manageIndicators(List<IndicatorRq> indicators, String documentNumber) throws Exception {
        LOGGER.info("manageIndicators() -> indicators,  documentNumber: {}, {}", GSON.toJson(indicators), documentNumber);

        Optional<User> optionalUser = BDFake.findByDocumentNumber(documentNumber);

        if (!optionalUser.isPresent())
            //throw new Exception("Usuario No Existe");
            return Collections.emptyList();

        if (CollectionUtils.isNotEmpty(indicators)) {

            if (indicators.stream().mapToInt(IndicatorRq::getEquivalentPercentage).sum() != 100)
                throw new Exception("El total de porcentaje equivalente debe ser 100%");

            List<Indicator> listIndicators = new ArrayList<>(indicators.size());
            indicators.forEach(indicatorRq -> {
                listIndicators.add(Indicator.builder()
                        .id(UUID.randomUUID().toString())
                        .name(indicatorRq.getName())
                        .equivalentPercentage(indicatorRq.getEquivalentPercentage())
                        .hexColor(indicatorRq.getHexColor())
                        .build()
                );
            });

            optionalUser.get().setIndicators(listIndicators);
            BDFake.addUser(optionalUser.get());

            return optionalUser.get().getIndicators()
                    .stream()
                    .map(this::parseIndicatorRs)
                    .sorted(Comparator.comparingDouble(IndicatorRs::getEquivalentPercentage))
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    @Override
    public List<IndicatorRs> getAllIndicators(String documentNumber) throws Exception {
        LOGGER.info("getAllIndicators() -> documentNumber: {}", documentNumber);

        Optional<User> optionalUser = BDFake.findByDocumentNumber(documentNumber);

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            if (CollectionUtils.isNotEmpty(user.getIndicators())) {
                return user.getIndicators()
                        .stream()
                        .map(this::parseIndicatorRs)
                        .sorted(Comparator.comparingDouble(IndicatorRs::getEquivalentPercentage))
                        .collect(Collectors.toList());
            }
        }

        return Collections.emptyList();
    }

    private IndicatorRs parseIndicatorRs(Indicator indicator) {
        return IndicatorRs.builder()
                .id(indicator.getId())
                .name(indicator.getName())
                .equivalentPercentage(indicator.getEquivalentPercentage())
                .hexColor(indicator.getHexColor())
                .build();
    }

}

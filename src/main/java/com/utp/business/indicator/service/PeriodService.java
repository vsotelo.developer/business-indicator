package com.utp.business.indicator.service;

import com.utp.business.indicator.controller.expose.IndicatorValueRs;
import com.utp.business.indicator.controller.expose.IndicatorValueUpdateRequest;
import com.utp.business.indicator.controller.expose.PeriodAverageRs;
import com.utp.business.indicator.controller.expose.PeriodRequest;
import com.utp.business.indicator.model.Period;

import java.util.List;

public interface PeriodService {

    Period insertPeriod(PeriodRequest periodRequest, String documentNumber) throws Exception;
    List<Period> getAllPeriods(String documentNumber) throws Exception;
    boolean updateMasiveIndicators(List<IndicatorValueUpdateRequest> indicatorValueUpdateRequests, String documentNumber) throws Exception;
    List<PeriodAverageRs> listAverage(String documentNumber) throws Exception;
    List<IndicatorValueRs> listValuesIndicatorsByPeriodId(String documentNumber, String periodId) throws Exception;
}
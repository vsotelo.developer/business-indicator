package com.utp.business.indicator.service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.utp.business.indicator.controller.expose.WorkerRs;
import com.utp.business.indicator.factory.BDFake;
import com.utp.business.indicator.model.User;
import com.utp.business.indicator.model.Worker;
import com.utp.business.indicator.service.WorkerService;
import com.utp.business.indicator.util.ServiceConstants;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class WorkerServiceImpl implements WorkerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkerServiceImpl.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(ServiceConstants.LOGGER_FORMAT_DATE).create();

    public static final String FILE_XLS = "xls";
    public static final String FILE_XLSX = "xlsx";

    @Override
    public File downloadFormatWorker() throws Exception {
        LOGGER.info("downloadFormatWorker()");
        return new File("etc/worker_format.xlsx");
    }

    @Override
    public List<WorkerRs> importFormatWorker(MultipartFile file, String documentNumber) throws Exception {
        LOGGER.info("importFormatWorker() -> documentNumber: {}", documentNumber);

        Optional<User> optionalUser = BDFake.findByDocumentNumber(documentNumber);
        if (!optionalUser.isPresent())
            throw new Exception("Usuario No Existe");

        if (file != null && file.getOriginalFilename() != null) {

            String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
            if (extension.equalsIgnoreCase(FILE_XLSX) || extension.equalsIgnoreCase(FILE_XLS)) {

                Workbook workbook = WorkbookFactory.create(file.getInputStream());

                List<Worker> workers = readExcel(workbook);

                optionalUser.get().setWorkers(workers);
                BDFake.addUser(optionalUser.get());

                return workers.stream().map(this::parseWorker)
                        .collect(Collectors.toList());

            } else {
                throw new Exception("[ERROR FORMAT FILE]");
            }
        }


        return Collections.emptyList();
    }

    @Override
    public List<WorkerRs> getListWorkers(String documentNumber) throws Exception {
        LOGGER.info("getListWorkers() -> documentNumber: {}", documentNumber);

        Optional<User> optionalUser = BDFake.findByDocumentNumber(documentNumber);
        if (!optionalUser.isPresent())
            //throw new Exception("Usuario No Existe");
            return Collections.emptyList();

        if (CollectionUtils.isNotEmpty(optionalUser.get().getWorkers())) {
            return optionalUser.get().getWorkers().stream().map(this::parseWorker)
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    private WorkerRs parseWorker(Worker worker) {
        return WorkerRs.builder()
                .id(UUID.randomUUID().toString())
                .documentNumber(worker.getDocumentNumber())
                .name(worker.getName())
                .email(worker.getEmail())
                .build();
    }

    private List<Worker> readExcel(Workbook workbook) throws Exception {
        long _ini = System.currentTimeMillis();

        List<Worker> workers = new ArrayList<>();
        Sheet _sheet = workbook.getSheetAt(NumberUtils.INTEGER_ZERO);
        Iterator<Row> _row_iterator = _sheet.iterator();
        Row _row;

        try {

            while (_row_iterator.hasNext()) {

                _row = _row_iterator.next();
                if (_row.getRowNum() > 0 && _row.getPhysicalNumberOfCells() > 0) {

                    Cell cellNumberDocument = _row.getCell(0);
                    Cell cellFullName = _row.getCell(1);
                    Cell cellEmail = _row.getCell(2);

                    workers.add(Worker.builder()
                            .id(UUID.randomUUID().toString())
                            .documentNumber(getValueCell(cellNumberDocument))
                            .name(getValueCell(cellFullName))
                            .email(getValueCell(cellEmail))
                            .build());
                }
            }

            return workers;
        } catch (Exception e) {
            throw new Exception("Error al leer excel");
        } finally {
            long end = System.currentTimeMillis();
            System.out.println("[PROCESS TIME] -> " + (end - _ini));
        }
    }

    public static String getValueCell(Cell cell) {
        return cell != null ? cell.toString() : StringUtils.EMPTY;
    }
}

package com.utp.business.indicator.service;

import com.utp.business.indicator.controller.expose.AuthResponse;

public interface UserService {

    AuthResponse auth(String documentNumber) throws Exception;
}

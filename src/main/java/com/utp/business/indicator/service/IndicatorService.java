package com.utp.business.indicator.service;

import com.utp.business.indicator.controller.expose.IndicatorRq;
import com.utp.business.indicator.controller.expose.IndicatorRs;

import java.util.List;

public interface IndicatorService {

    List<IndicatorRs> manageIndicators(List<IndicatorRq> indicators, String documentNumber) throws Exception;
    List<IndicatorRs> getAllIndicators(String documentNumber) throws Exception;
}

package com.utp.business.indicator.service;

import com.utp.business.indicator.controller.expose.WorkerRs;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

public interface WorkerService {

    File downloadFormatWorker() throws Exception;
    List<WorkerRs> importFormatWorker(MultipartFile file, String documentNumber) throws Exception;
    List<WorkerRs> getListWorkers(String documentNumber) throws Exception;
}

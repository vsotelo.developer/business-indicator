package com.utp.business.indicator.service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.utp.business.indicator.controller.UserController;
import com.utp.business.indicator.controller.expose.AuthResponse;
import com.utp.business.indicator.factory.BDFake;
import com.utp.business.indicator.model.User;
import com.utp.business.indicator.service.UserService;
import com.utp.business.indicator.util.ServiceConstants;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(ServiceConstants.LOGGER_FORMAT_DATE).create();

    @Override
    public AuthResponse auth(String documentNumber) throws Exception {
        LOGGER.info("auth() -> documentNumber : {}", documentNumber);

        try {

            Optional<User> optionalUser = BDFake.findByDocumentNumber(documentNumber);
            User user;

            if (optionalUser.isPresent()) {
                user = optionalUser.get();
            } else {
                user = User.builder()
                        .id(UUID.randomUUID().toString())
                        .documentNumber(documentNumber)
                        .build();
                BDFake.addUser(user);
            }

            Date now = new Date();
            Date validity = new Date(now.getTime() + 31536000000L);

            Map<String, Object> claims = new HashMap<>();

            claims.put("globalId", user.getId());
            claims.put("documentNumber", user.getDocumentNumber());

            return AuthResponse.builder()
                    .token(
                            Jwts.builder()
                                    .setSubject(documentNumber)
                                    .setClaims(claims)
                                    .setIssuedAt(now)
                                    .setExpiration(validity)
                                    .signWith(SignatureAlgorithm.HS512, ServiceConstants.SECRET_KEY)
                                    .compact()
                    ).build();

        } catch (Exception e) {
            throw new Exception("error.service.auth");
        }
    }
}

package com.utp.business.indicator.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class User {

    // key
    private String id;

    // Attributes
    private String documentNumber;

    // Relations
    private List<Indicator> indicators;
    private List<Worker> workers;
    private List<Period> periods;

}

package com.utp.business.indicator.model;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class PeriodDetail {

    // key
    private String id;

    // Attributes
    private Date date;

    // Relations
    private List<WorkerDetailPeriod> workerDetailPeriods;

    public double getAverageByDays() {
        return workerDetailPeriods.stream()
                .mapToDouble(WorkerDetailPeriod::getValueAverage)
                .sum() / workerDetailPeriods.size();
    }
}

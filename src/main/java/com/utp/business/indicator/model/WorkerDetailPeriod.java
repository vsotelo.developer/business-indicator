package com.utp.business.indicator.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class WorkerDetailPeriod {

    // key
    private String id;

    // Attributes
    private Worker worker;

    // Relations
    private List<IndicatorValue> indicatorValues;

    public double getValueAverage() {
        return indicatorValues.stream()
                .mapToDouble(IndicatorValue::getValorIndicatorEquivalent)
                .sum();
    }

}

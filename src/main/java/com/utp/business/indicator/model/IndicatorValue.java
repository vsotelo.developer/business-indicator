package com.utp.business.indicator.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IndicatorValue {

    // key
    private String id;

    // Attributes
    private double value;

    // Relations
    private Indicator indicator;

    public double getValorIndicatorEquivalent() {
        return (value * indicator.getEquivalentPercentage()) / 100;
    }

}

package com.utp.business.indicator.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Indicator {

    // key
    private String id;

    // Attributes 
    private String name;
    private double equivalentPercentage;
    private String hexColor;

}

package com.utp.business.indicator.model;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Data
@Builder
public class Period {

    // key
    private String id;

    // Attributes
    private String name;
    private Date dateInit;
    private Date dateEnd;

    private List<Indicator> listIndicatorsPeriod;

    // Relations
    private List<PeriodDetail> periodDetails;

    public double getAverageByDays() {
        long diffInMillies = Math.abs(dateEnd.getTime() - dateInit.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS) + 1;
        return periodDetails.stream()
                .mapToDouble(PeriodDetail::getAverageByDays)
                .sum() / diff;
    }

}

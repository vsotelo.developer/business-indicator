package com.utp.business.indicator.util;

import com.utp.business.indicator.controller.expose.ObjectResponse;

public class ResponseUtil {

    public static <R> ObjectResponse<R> createResponse(String message, R result) {
        ObjectResponse<R> _result = new ObjectResponse<>();
        _result.setMessage(message);
        _result.setResult(result);
        return _result;
    }
}

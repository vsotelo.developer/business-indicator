package com.utp.business.indicator.util;

public class ServiceConstants {

    public static final String LOGGER_FORMAT_DATE = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String SECRET_KEY = "ZGJfaGVhbHRoeWZvb2RzcG9ydA==";
    public static final String APPLICATION = "application";
    public static final String APPLICATION_SUBTYPE = "vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String HEADER_WORKER_XLS = "attachment;filename=worker_format.xlsx";

}

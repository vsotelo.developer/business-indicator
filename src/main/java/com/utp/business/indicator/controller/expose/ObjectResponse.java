package com.utp.business.indicator.controller.expose;


import lombok.Data;

@Data
public class ObjectResponse<T> {

    private String message;
    private T result;

}
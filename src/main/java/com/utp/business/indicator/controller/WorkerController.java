package com.utp.business.indicator.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.utp.business.indicator.controller.expose.ObjectResponse;
import com.utp.business.indicator.controller.expose.WorkerRs;
import com.utp.business.indicator.service.WorkerService;
import com.utp.business.indicator.util.ResponseUtil;
import com.utp.business.indicator.util.ServiceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;

@RestController
@RequestMapping("/worker")
public class WorkerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkerController.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(ServiceConstants.LOGGER_FORMAT_DATE).create();

    private final WorkerService workerService;

    @Autowired
    public WorkerController(WorkerService workerService) {
        this.workerService = workerService;
    }

    @GetMapping("/download/format/")
    public ResponseEntity<InputStreamResource> downloadFormatWorker(HttpServletResponse response) throws Exception {
        File file = workerService.downloadFormatWorker();

        MediaType APPLICATION_XLS = new MediaType(ServiceConstants.APPLICATION, ServiceConstants.APPLICATION_SUBTYPE);
        FileInputStream fileInputStream = new FileInputStream(file);
        InputStreamResource inputStreamResource = new InputStreamResource(fileInputStream);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, ServiceConstants.HEADER_WORKER_XLS)
                .contentType(APPLICATION_XLS)
                .contentLength(file.length())
                .body(inputStreamResource);
    }

    @PostMapping("/import")
    public ResponseEntity<ObjectResponse<List<WorkerRs>>> importFormatWorker(@RequestPart MultipartFile file, ServletRequest requestToken) throws Exception {
        final HttpServletRequest req = (HttpServletRequest) requestToken;
        final String documentNumber = (String) req.getAttribute("documentNumber");
        LOGGER.info("importFormatWorker() -> documentNumber : {}", GSON.toJson(documentNumber));
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                workerService.importFormatWorker(file, documentNumber)), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<ObjectResponse<List<WorkerRs>>> getListWorkers(ServletRequest requestToken) throws Exception {
        final HttpServletRequest req = (HttpServletRequest) requestToken;
        final String documentNumber = (String) req.getAttribute("documentNumber");
        LOGGER.info("getListWorkers() -> documentNumber : {}", GSON.toJson(documentNumber));
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                workerService.getListWorkers(documentNumber)), HttpStatus.OK);
    }
}

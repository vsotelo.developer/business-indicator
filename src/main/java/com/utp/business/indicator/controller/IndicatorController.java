package com.utp.business.indicator.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.utp.business.indicator.controller.expose.IndicatorRq;
import com.utp.business.indicator.controller.expose.IndicatorRs;
import com.utp.business.indicator.controller.expose.ObjectResponse;
import com.utp.business.indicator.service.IndicatorService;
import com.utp.business.indicator.util.ResponseUtil;
import com.utp.business.indicator.util.ServiceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/indicator")
public class IndicatorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndicatorController.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(ServiceConstants.LOGGER_FORMAT_DATE).create();
    
    private final IndicatorService indicatorService;

    @Autowired
    public IndicatorController(IndicatorService indicatorService) {
        this.indicatorService = indicatorService;
    }

    @PostMapping("/manage/")
    public ResponseEntity<ObjectResponse<List<IndicatorRs>>> manageIndicators(@RequestBody List<IndicatorRq> indicators, ServletRequest requestToken) throws Exception {
        LOGGER.info("auth() -> manageIndicators : {}", GSON.toJson(indicators));
        final HttpServletRequest req = (HttpServletRequest) requestToken;
        final String documentNumber = (String) req.getAttribute("documentNumber");
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                indicatorService.manageIndicators(indicators, documentNumber)), HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<ObjectResponse<List<IndicatorRs>>> getAllIndicators(ServletRequest requestToken) throws Exception {
        final HttpServletRequest req = (HttpServletRequest) requestToken;
        final String documentNumber = (String) req.getAttribute("documentNumber");
        LOGGER.info("getAllIndicators() -> documentNumber : {}", GSON.toJson(documentNumber));
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                indicatorService.getAllIndicators(documentNumber)), HttpStatus.OK);
    }
}

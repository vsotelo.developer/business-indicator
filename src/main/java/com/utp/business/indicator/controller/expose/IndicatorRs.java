package com.utp.business.indicator.controller.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IndicatorRs {

    private String id;
    private String name;
    private double equivalentPercentage;
    private String hexColor;
}

package com.utp.business.indicator.controller.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WorkerRs {

    private String id;
    private String documentNumber;
    private String name;
    private String email;
}

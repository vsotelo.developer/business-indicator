package com.utp.business.indicator.controller.expose;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class PeriodDetailResponse {

    private String id;
    private Date date;

    private PeriodResponse period;

}

package com.utp.business.indicator.controller.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthResponse {

    private String token;
}

package com.utp.business.indicator.controller.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PeriodAverageRs {

    private String periodId;
    private int index;
    private Double value;
    private String periodName;

}

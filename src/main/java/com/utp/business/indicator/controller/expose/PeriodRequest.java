package com.utp.business.indicator.controller.expose;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class PeriodRequest {

    private String name;
    private String dateInit;
    private String dateEnd;

}

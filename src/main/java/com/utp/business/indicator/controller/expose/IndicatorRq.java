package com.utp.business.indicator.controller.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IndicatorRq {

    private String name;
    private Integer equivalentPercentage;
    private String hexColor;
}

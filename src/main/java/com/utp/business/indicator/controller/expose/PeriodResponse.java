package com.utp.business.indicator.controller.expose;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class PeriodResponse {

    private String id;
    private String name;
    private Date dateInit;
    private Date dateEnd;
    private List<PeriodDetailResponse> periodDetails;
}

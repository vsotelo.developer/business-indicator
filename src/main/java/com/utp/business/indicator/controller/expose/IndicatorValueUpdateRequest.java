package com.utp.business.indicator.controller.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IndicatorValueUpdateRequest {

    private String periodId;
    private String periodDetailId;
    private String workerPeriodDetailId;
    private String indicatorValueId;
    private double value;

}

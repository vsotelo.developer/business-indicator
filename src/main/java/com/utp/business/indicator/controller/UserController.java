package com.utp.business.indicator.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.utp.business.indicator.controller.expose.AuthResponse;
import com.utp.business.indicator.controller.expose.ObjectResponse;
import com.utp.business.indicator.service.UserService;
import com.utp.business.indicator.util.ResponseUtil;
import com.utp.business.indicator.util.ServiceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(ServiceConstants.LOGGER_FORMAT_DATE).create();

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/auth/")
    public ResponseEntity<ObjectResponse<AuthResponse>> auth(@RequestParam String documentNumber) throws Exception {
        LOGGER.info("auth() -> user : {}", documentNumber);
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                userService.auth(documentNumber)), HttpStatus.OK);
    }
}

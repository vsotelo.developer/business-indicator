package com.utp.business.indicator.controller.expose;

import com.utp.business.indicator.model.Indicator;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IndicatorValueRs {

    private Integer index;
    private Indicator indicator;
    private Double average;
}

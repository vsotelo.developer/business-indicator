package com.utp.business.indicator.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.utp.business.indicator.controller.expose.*;
import com.utp.business.indicator.model.Period;
import com.utp.business.indicator.service.PeriodService;
import com.utp.business.indicator.util.ResponseUtil;
import com.utp.business.indicator.util.ServiceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/period")
public class PeriodController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PeriodController.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(ServiceConstants.LOGGER_FORMAT_DATE).create();

    private final PeriodService periodService;

    @Autowired
    public PeriodController(PeriodService periodService) {
        this.periodService = periodService;
    }


    @GetMapping("/")
    public ResponseEntity<ObjectResponse<List<Period>>> getAllPeriods(ServletRequest requestToken) throws Exception {
        final HttpServletRequest req = (HttpServletRequest) requestToken;
        final String documentNumber = (String) req.getAttribute("documentNumber");
        LOGGER.info("getAllPeriods() -> documentNumber : {}", GSON.toJson(documentNumber));
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                periodService.getAllPeriods(documentNumber)), HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<ObjectResponse<Period>> insertPeriod(@RequestBody PeriodRequest period, ServletRequest requestToken) throws Exception {
        LOGGER.info("auth() -> managePeriod : {}", GSON.toJson(period));
        final HttpServletRequest req = (HttpServletRequest) requestToken;
        final String documentNumber = (String) req.getAttribute("documentNumber");
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                periodService.insertPeriod(period, documentNumber)), HttpStatus.OK);
    }

    @PostMapping("/update/masive")
    public ResponseEntity<ObjectResponse<Boolean>> updateMasiveIndicators(@RequestBody List<IndicatorValueUpdateRequest> indicatorValueUpdateRequests, ServletRequest requestToken) throws Exception {
        LOGGER.info("updateMasiveIndicators : {}", GSON.toJson(indicatorValueUpdateRequests));
        final HttpServletRequest req = (HttpServletRequest) requestToken;
        final String documentNumber = (String) req.getAttribute("documentNumber");
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                periodService.updateMasiveIndicators(indicatorValueUpdateRequests, documentNumber)), HttpStatus.OK);
    }

    @GetMapping("/average")
    public ResponseEntity<ObjectResponse<List<PeriodAverageRs>>> getAveragePeriod(ServletRequest requestToken) throws Exception {
        LOGGER.info("getAveragePeriod");
        final HttpServletRequest req = (HttpServletRequest) requestToken;
        final String documentNumber = (String) req.getAttribute("documentNumber");
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                periodService.listAverage(documentNumber)), HttpStatus.OK);
    }

    @GetMapping("/average/indicators")
    public ResponseEntity<ObjectResponse<List<IndicatorValueRs>>> listValuesIndicatorsByPeriodId(ServletRequest requestToken
            , @RequestParam String periodId) throws Exception {
        LOGGER.info("listValuesIndicatorsByPeriodId");
        final HttpServletRequest req = (HttpServletRequest) requestToken;
        final String documentNumber = (String) req.getAttribute("documentNumber");
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                periodService.listValuesIndicatorsByPeriodId(documentNumber, periodId)), HttpStatus.OK);
    }
}

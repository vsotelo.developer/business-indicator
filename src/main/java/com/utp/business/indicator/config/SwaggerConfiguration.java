package com.utp.business.indicator.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    private static final String BASE_PACKAGE = "com.utp.business.indicator.controller";
    private static final String ACCESS = "accessEverything";
    private static final String GLOBAL = "global";
    private static final String HEADER = "header";
    private static final String JWT = "JWT";

    @Value("${application.name}")
    public String applicationName;

    @Value("${application.description}")
    public String applicationDescription;

    @Value("${build.version}")
    public String buildVersion;

    @Value("${build.branch}")
    public String buildBranch;

    @Value("${build.commit}")
    public String buildCommit;

    @Value("${build.timestamp}")
    public String buildTimestamp;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .securityContexts(Collections.singletonList(securityContext()))
                .securitySchemes(Collections.singletonList(apiKey()))
                .genericModelSubstitutes(ResponseEntity.class)
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("\nDescription : ").append(applicationDescription);
        stringBuilder.append("\n");
        stringBuilder.append("\nBuild commit     : ").append(buildCommit);
        stringBuilder.append("\nBuild branch     : ").append(buildBranch);
        stringBuilder.append("\nBuild timestamp  : ").append(buildTimestamp);

        return new ApiInfoBuilder()
                .title(applicationName)
                .description(stringBuilder.toString())
                .version(buildVersion)
                .build();
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope(GLOBAL, ACCESS);
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Collections.singletonList(new SecurityReference(JWT, authorizationScopes));
    }

    private ApiKey apiKey() {
        return new ApiKey(JWT, HttpHeaders.AUTHORIZATION, HEADER);
    }


}

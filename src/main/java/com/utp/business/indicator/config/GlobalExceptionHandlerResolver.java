package com.utp.business.indicator.config;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandlerResolver {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<CustomResponseException> customException(Exception e, ServletWebRequest servletWebRequest) {
        e.printStackTrace();
        CustomResponseException responseException =
                CustomResponseException.builder()
                        .timestamp(LocalDateTime.now())
                        .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        .error(e.getMessage())
                        .path(servletWebRequest.getRequest().getRequestURI())
                        .build();
        return new ResponseEntity<>(responseException, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Builder
    @Data
    private static class CustomResponseException {

        private LocalDateTime timestamp;
        private int status;
        private String error;
        private String path;

    }
}

package com.utp.business.indicator.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.utp.business.indicator.controller.UserController;
import com.utp.business.indicator.util.ServiceConstants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Optional;

@Component
@Order(1)
public class SecurityAuthenticationFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(ServiceConstants.LOGGER_FORMAT_DATE).create();

    public static final String APPLICATION_JSON = "application/json";
    public static final String UTF8 = "UTF-8";
    public static final String KEY_CODE = "codeError";
    public static final String KEY_MESSAGE = "messageError";
    public static final String KEY_HTTP_STATUS = "httpStatus";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) servletRequest;
        final HttpServletResponse res = (HttpServletResponse) servletResponse;

        if (StringUtils.containsAny(req.getRequestURI(), "swagger", "api-docs", "auth")) {
            filterChain.doFilter(req, res);
            return;
        }

        final String authHeader = Optional.ofNullable(req.getHeader(HttpHeaders.AUTHORIZATION)).orElse(StringUtils.EMPTY);

        if (authHeader.isEmpty()) {
            generateResult(res, HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN.toString(), "Token is required");
            return;
        }

        Claims claims = Jwts.parser().setSigningKey(ServiceConstants.SECRET_KEY).parseClaimsJws(authHeader).getBody();

        String globalId = MapUtils.getString(claims, "globalId");
        String documentNumber = MapUtils.getString(claims, "documentNumber");

        setterHeader(globalId, documentNumber, res, req);
        filterChain.doFilter(req, res);

    }

    @Override
    public void destroy() {
    }

    public static void generateResult(HttpServletResponse httpServletResponse, Integer status, String code, String message) {
        try {

            httpServletResponse.setStatus(status);
            httpServletResponse.setContentType(APPLICATION_JSON);
            httpServletResponse.setCharacterEncoding(UTF8);

            LinkedHashMap<String, Object> mapError = new LinkedHashMap<>();

            mapError.put(KEY_CODE, code);
            mapError.put(KEY_MESSAGE, message);
            mapError.put(KEY_HTTP_STATUS, status);

            PrintWriter out = httpServletResponse.getWriter();
            out.print(GSON.toJson(mapError));
            out.flush();
            IOUtils.closeQuietly(out);

        } catch (IOException exception) {
            LOGGER.error("generateResult() -> Exception : {}", exception.getMessage());
        }

    }


    private void setterHeader(String globalId, String documentNumber, HttpServletResponse res, HttpServletRequest req) {
        // Headers
        res.setContentType(APPLICATION_JSON);
        res.setCharacterEncoding(UTF8);
        res.setStatus(HttpServletResponse.SC_OK);

        res.addHeader("globalId", globalId);
        res.addHeader("documentNumber", documentNumber);

        // Attributes
        req.setAttribute("globalId", globalId);
        req.setAttribute("documentNumber", documentNumber);

    }

}

#RUN JAVA 8 Desktop:
* mvn clean package
* cd/ target
* java -jar *.jar

#RUN CONTAINER
* docker build . -t business-indicator-app
* docker run -p 5202:5202 business-indicator-app/business-indicator-app

#TEMP: 
- https://grupo1-utp.linkfast.com.pe/business-indicator/swagger-ui/
